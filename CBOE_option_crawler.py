# -*- coding: utf-8 -*-
"""
Created on Wed Jun 22 16:25:25 2022

@author: Hana_FI
"""

from selenium import webdriver
import os
import time
import pyautogui
import shutil
import pandas as pd
import pygetwindow as gw
# import os
# now_path = os.path.abspath('.')
# os.chdir(r'C:\Users\Hana_FI\pythonProject\MNS')
# import myDB
# os.chdir(now_path)

CBOE_URL = 'https://www.cboe.com/delayed_quotes/{code}/quote_table'
xpath_option_range_selector = '/html/body/main/section[1]/div/div/div/div/div[2]/div[2]/div[2]/div[2]/div[3]/div/div[2]/div/div/div'
xpath_expiration_selector = '/html/body/main/section[1]/div/div/div/div/div[2]/div[2]/div[2]/div[2]/div[4]/div/div[2]/div/div/div/div[1]'
xpath_view_chain_btn = '/html/body/main/section[1]/div/div/div/div/div[2]/div[2]/div[2]/div[2]/div [5]/div/button/span'
xpath_expiration_btn = '/html/body/main/section[1]/div/div/div/div/div[2]/div[2]/div[3]/div[9]'
xpath_cboe_date = '/html/body/main/section[1]/div/div/div/div/div[2]/div[2]/div[1]/div[3]/div[1]/div'


class_name_download_btn = 'Button__TextContainer-cui__sc-1ahwe65-1.fwXyiw'
xpath_download_btn = '/html/body/main/section[1]/div/div/div/div/div[2]/div[2]/div[3]/div[54]/a/span[1]'

option_DATA_PATH = r'D:\QuantTeam\Dataset\CBOE_option'

# os.chdir(r'C:\Users\Hana_FI\pythonProject\crawler')
# driver = webdriver.Chrome('chromedriver')
# driver.get(CBOE_URL.format(code='spx'))

# crawler_win = pyautogui.getWindowsWithTitle('SPX')[0]
# crawler_win.activate()
# if crawler_win.isActive == False:
#     crawler_win.activate()
# if crawler_win.isMaximized == False:
#     crawler_win.maximize()
    
# while True:
#     try:
#         time.sleep(1)
#         #driver.find_element_by_css_selector('css_selector_of_element')
#         option_range_selector = driver.find_element_by_xpath(xpath_option_range_selector)
#         expiration_selector = driver.find_element_by_xpath(xpath_expiration_selector)
        
#          # All로 변경하는 작업 수행. 이부분이 애매하구먼?
#         if option_range_selector.text != 'All':
#             print('option_range_selector Try')
#             pyautogui.getWindowsWithTitle('SPX')[0].activate()
#             pyautogui.click(300,300)
#             option_range_selector.click()
#             time.sleep(1)
#             pyautogui.press('up')
#             time.sleep(1)
#             pyautogui.press('space')
#             print(f'option_range_selector.text : {option_range_selector.text}')
            
#         if expiration_selector.text != 'All':
#             print('expiration_selector Try')
#             pyautogui.getWindowsWithTitle('SPX')[0].activate()
#             pyautogui.click(300,300)
#             expiration_selector.click()
#             time.sleep(1)
#             pyautogui.press('up')
#             time.sleep(1)
#             pyautogui.press('space')
#             print(f'expiration_selector.text : {expiration_selector.text}')
            
#         #select = Select(option_range_selector)
#         #select.select_by_visible_text('All')
#         if (option_range_selector.text == 'All') & (expiration_selector.text == 'All'):
#             view_chain_btn = driver.find_element_by_xpath(xpath_view_chain_btn)
#             view_chain_btn.click()
#             time.sleep(10)
#             break
        
#     except Exception as e:
#         print(e, type(e))

# print('화면 초기화 완료')
# cboe_date = driver.find_element_by_xpath(xpath_cboe_date).text.split(' ')[:-2]
# file_name = ('_'.join(cboe_date + ['option']) + '.csv').replace(':','')
# if crawler_win.isActive == False:
#     crawler_win.activate()
# pyautogui.hotkey('ctrl', 'end')
# [pyautogui.press('up') for i in range(20)]

# while True:
#     try:
#         time.sleep(1)
#         download_csv_btn = driver.find_element_by_class_name(class_name_download_btn)
#         #download_csv_btn = driver.find_element_by_xpath(xpath_download_btn)
#         download_csv_btn.click()
#         break
#     except Exception as e:
#         print(e, type(e))

# # 다운로드 대기까지
# time.sleep(3)

# download_path = r'C:\Users\Hana_FI\Downloads'
# to_path = r'\\10.155.31.53\Share\QuantTeam\Dataset\CBOE_option'

# if os.path.isdir(to_path) == False:
#     os.mkdir(to_path)
# target_file = [ file  for file in os.listdir(download_path) if ('spx_quotedata' in file) and ('.csv' in file)]
# shutil.move(os.path.join(download_path, target_file[0]), os.path.join(to_path, file_name))


def download_data_cboe(code):
    driver = webdriver.Chrome(r'C:\Users\Hana_FI\pythonProject\crawler\chromedriver')
    driver.get(CBOE_URL.format(code=code))
    driver.maximize_window()
    
    while True:
        try:
            crawler_win = pyautogui.getWindowsWithTitle(code)[0]
            
            if crawler_win.isMinimized == True:
                crawler_win.restore()

            if crawler_win.isActive == False:
                crawler_win.activate()
                
            [pyautogui.press('down') for i in range(4)]
            print('window move')
            break
        except Exception as e:
            print(e, type(e))
            print('Retry')
            time.sleep(1)
    
    
    while True:
        try:
            time.sleep(1)
            #driver.find_element_by_css_selector('css_selector_of_element')
            option_range_selector = driver.find_element_by_xpath(xpath_option_range_selector)
            expiration_selector = driver.find_element_by_xpath(xpath_expiration_selector)
            
             # All로 변경하는 작업 수행. 이부분이 애매하구먼?
            if option_range_selector.text != 'All':
                print('option_range_selector Try')
                pyautogui.getWindowsWithTitle(code)[0].activate()
                pyautogui.click(300,300)
                option_range_selector.click()
                time.sleep(1)
                pyautogui.press('up')
                time.sleep(1)
                pyautogui.press('space')
                print(f'option_range_selector.text : {option_range_selector.text}')
                
            if expiration_selector.text != 'All':
                print('expiration_selector Try')
                pyautogui.getWindowsWithTitle(code)[0].activate()
                pyautogui.click(300,300)
                expiration_selector.click()
                time.sleep(1)
                pyautogui.press('up')
                time.sleep(1)
                pyautogui.press('space')
                print(f'expiration_selector.text : {expiration_selector.text}')
                
            #select = Select(option_range_selector)
            #select.select_by_visible_text('All')
            if (option_range_selector.text == 'All') & (expiration_selector.text == 'All'):
                view_chain_btn = driver.find_element_by_xpath(xpath_view_chain_btn)
                view_chain_btn.click()
                time.sleep(10)
                break
            
        except Exception as e:
            print(e, type(e))
            time.sleep(1)

    print('화면 초기화 완료')
    while True:
        try:
            cboe_date = driver.find_element_by_xpath(xpath_cboe_date).text.split(' ')[:-2]
            if crawler_win.isActive == False:
                crawler_win.activate()
            pyautogui.hotkey('ctrl', 'end')
            [pyautogui.press('up') for i in range(20)]
            break
        except Exception as e:
            print(e, type(e))
            print('Retry Find it')
            time.sleep(1)
            
    file_name = ('_'.join(cboe_date + [code,'option']) + '.csv').replace(':','')

    while True:
        try:
            time.sleep(1)
            download_csv_btn = driver.find_element_by_class_name(class_name_download_btn)
            
            # driver.find_element_by_class_name('Button__TextContainer-cui__sc-1ahwe65-1.fwXyiw')
            class_name_download_btn
            # download_csv_btn = driver.find_element_by_xpath(xpath_download_btn)
            download_csv_btn.click()
            break
        except Exception as e:
            print(e, type(e))
            print('Retry Find it')
            time.sleep(1)

    # 다운로드 대기까지
    time.sleep(3)

    download_path = r'C:\Users\Hana_FI\Downloads'
    

    if os.path.isdir(option_DATA_PATH) == False:
        os.mkdir(option_DATA_PATH)
        
    target_file = [ file  for file in os.listdir(download_path) if (f'{code}_quotedata' in file) and ('.csv' in file)]
    shutil.move(os.path.join(download_path, target_file[0]), os.path.join(option_DATA_PATH, file_name))
    driver.close()
    return os.path.join(option_DATA_PATH, file_name)

if __name__ == '__main__':
    # download_data_cboe('spx')
    for item in ['spx', 'vix', 'ndx']:
        file_name = download_data_cboe(item)
        
    # download_data_cboe('spx')
    # download_data_cboe('vix')
    # download_data_cboe('ndx')
    
    
    